'use strict';

rockPaperScissorsApp.controller('cpuVsCpuCtrl', ['commonCodeService', '$timeout', function(commonCodeService, $timeout) {
	var cpuVsCpuCtrl = this;

	cpuVsCpuCtrl.commenceCPUGame = function(){
		var randomNumber1 = commonCodeService.generateRandomNumber(),
			randomNumber2 = commonCodeService.generateRandomNumber();		

		// generate random first computer choice based on random number value
		cpuVsCpuCtrl.computer1Selection = commonCodeService.randomComputerChoice(randomNumber1);		

		// intentional second cpu response delay for improved ui
		$timeout(function(){	
			cpuVsCpuCtrl.computer2Selection = commonCodeService.randomComputerChoice(randomNumber2);				
		}, 1500).then( function(){		
			// compare cpu v cpu selections and store game outcome on controller property
			cpuVsCpuCtrl.gameOutCome = commonCodeService.compareSelections(cpuVsCpuCtrl.computer1Selection, cpuVsCpuCtrl.computer2Selection);		
		});
	};

	cpuVsCpuCtrl.restartGame = function(){
		cpuVsCpuCtrl.computer1Selection = cpuVsCpuCtrl.computer2Selection = cpuVsCpuCtrl.gameOutCome = '';
	};
}]);