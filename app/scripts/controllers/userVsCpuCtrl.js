'use strict';

rockPaperScissorsApp.controller('userVsCpuCtrl', ['commonCodeService', '$timeout', function(commonCodeService, $timeout) {
	var userVsCpuCtrl = this;

	userVsCpuCtrl.enterUserSelection = function(choice){
		userVsCpuCtrl.userSelection = choice;

		var randomNumber = commonCodeService.generateRandomNumber();

		// intentional cpu response delay for improved ui
		$timeout(function(){		
			// generate random computer choice based on random number value			
			userVsCpuCtrl.computerSelection = commonCodeService.randomComputerChoice(randomNumber);
		}, 1500).then( function(){
			// compare user v cpu selections and store game outcome on controller property
			userVsCpuCtrl.gameOutCome = commonCodeService.compareSelections(userVsCpuCtrl.userSelection, userVsCpuCtrl.computerSelection);			
		});
	};

	// clear properties on controller
	userVsCpuCtrl.restartGame = function(){
		userVsCpuCtrl.userSelection = userVsCpuCtrl.computerSelection = userVsCpuCtrl.gameOutCome = '';		
	};
}]);