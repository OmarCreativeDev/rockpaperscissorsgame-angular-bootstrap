'use strict';

/**
 * @ngdoc service
 * @name rockPaperScissorsApp.commonCodeService
 * @description
 * # commonCodeService
 * Service in the rockPaperScissorsApp.
 */
rockPaperScissorsApp.service('commonCodeService', function() {
	var commonCodeService = this;

	commonCodeService.generateRandomNumber = function(){
		return Math.random();
	};

	commonCodeService.randomComputerChoice = function(randomNumber){
		if (randomNumber <= 0.33) {
			return 'Rock';
		}
		else if (randomNumber > 0.34 && randomNumber < 0.66) {
			return 'Paper';
		} 
		else {
			return 'Scissors';
		}
	};	

	commonCodeService.compareSelections = function(choice1, choice2){
		if (choice1 === choice2) {
			return 'Both choices are the same. Its a DRAW!';
		}
		else if (choice1 === 'Rock') {
			if (choice2 === 'Scissors') {
				return 'Rock wins';
			}
			else {
				return 'Paper wins';
			}
		}     
		else if (choice1 === 'Paper') {
			if (choice2 === 'Rock') {
				return 'Paper wins';
			}
			else {
				return 'Scissors wins';
			}
		}
		else if (choice1 === 'Scissors') {
			if (choice2 === 'Paper') {
				return 'Scissors wins';
			}
			else {
				return 'Rock wins';
			}
		}
	};
});