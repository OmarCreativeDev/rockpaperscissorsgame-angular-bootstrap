'use strict';

/**
 * @ngdoc overview
 * @name rockPaperScissorsApp
 * @description
 * # rockPaperScissorsApp
 *
 * Main module of the application.
 */
var rockPaperScissorsApp = angular.module('rockPaperScissorsApp', ['ngAnimate', 'ui.router']);

rockPaperScissorsApp.config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('/', {
			templateUrl: 'views/main.html',
			url: '/'
		})
		.state('userVsCpu', {
			templateUrl: 'views/userVsCpu.html',
			url: '/user-vs-cpu',
			controller: 'userVsCpuCtrl as userVsCpuCtrl'
		})
		.state('cpuVsCpu', {
			templateUrl: 'views/cpuVsCpu.html',
			url: '/cpu-vs-cpu',
			controller: 'cpuVsCpuCtrl as cpuVsCpuCtrl'
		});
});