'use strict';

describe('Controller: userVsCpuCtrl', function () {

  // load the controller's module
  beforeEach(module('rockPaperScissorsApp'));

  var userVsCpuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    userVsCpuCtrl = $controller('userVsCpuCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(userVsCpuCtrl.awesomeThings.length).toBe(3);
  });
});
