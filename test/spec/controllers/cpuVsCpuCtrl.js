'use strict';

describe('Controller: cpuVsCpuCtrl', function () {

  // load the controller's module
  beforeEach(module('rockPaperScissorsApp'));

  var cpuVsCpuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    cpuVsCpuCtrl = $controller('cpuVsCpuCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    // expect(cpuVsCpuCtrl.awesomeThings.length).toBe(3);
  });
});