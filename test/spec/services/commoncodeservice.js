'use strict';

describe('Service: commonCodeService', function () {

  // load the service's module
  beforeEach(module('rockPaperScissorsApp'));

  // instantiate service
  var commonCodeService;
  beforeEach(inject(function (_commonCodeService_) {
    commonCodeService = _commonCodeService_;
  }));

  it('should do something', function () {
    expect(!!commonCodeService).toBe(true);
  });

});
